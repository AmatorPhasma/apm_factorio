__Amator Phasma's Solar Addon__
=============
[Source Code on GitLab](https://gitlab.com/AmatorPhasma/apm_factorio)

__Languages:__
-------------
English, Deutsch

__About this mod:__
-------------
...

__Future?__
-------------
- None

__Problems?__
-------------
- Report them on the forum thread or here on this site under disscussion.

__Ideas?__
-------------
- Please tell me on the forum thread.

__Dependencies:__
-------------
- None

__Known incompatibility:__
-------------
- None

__My other mods:__
-------------
- [Amator Phasma's Modpack](https://mods.factorio.com/mod/apm_modpack)
- [Amator Phasma's Coal&Steam](https://mods.factorio.com/mod/apm_power)
- [Amator Phasma's Nuclear](https://mods.factorio.com/mod/apm_nuclear)
- [Amator Phasma's Recycling](https://mods.factorio.com/mod/apm_recycling)
- [Amator Phasma's Starfall](https://mods.factorio.com/mod/apm_starfall)
- [Amator Phasma's Energy Addon](https://mods.factorio.com/mod/apm_energy_addon)

[My thread in the forum.](https://forums.factorio.com/viewtopic.php?f=190&t=68748)