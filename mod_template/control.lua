-- Requires Defines------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local initial = require 'lib.initial'
local updates = require 'lib.updates'

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function on_init()
    initial.run()
    updates.run()
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function on_Load()
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function on_update()
    initial.run()
    updates.run()
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function event_on_tick(event)
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function event_mod_setting_changed(event)
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function event_on_player_created(event)
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function event_on_build(event)
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function event_on_robot_build(event)
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function event_on_mined(event)
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function event_on_robot_mined(event)
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function event_on_player_respawned(event)
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function event_on_player_rotated_entity(event)
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function event_on_entity_died(event)
end

-- Event Defines---------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
script.on_event(defines.events.on_player_created, function(event) event_on_player_created(event) end)
script.on_event(defines.events.on_player_respawned, function(event) event_on_player_respawned(event) end)

script.on_event(defines.events.on_tick, function(event) event_on_tick(event) end)
script.on_event (defines.events.on_runtime_mod_setting_changed, function(event) event_mod_setting_changed(event) end)

script.on_event(defines.events.on_built_entity, function(event) event_on_build(event) end)
script.on_event(defines.events.on_robot_built_entity, function(event) event_on_robot_build(event) end)

script.on_event(defines.events.on_player_mined_entity, function(event) event_on_mined(event) end)
script.on_event(defines.events.on_robot_mined_entity, function(event) event_on_robot_mined(event) end)

script.on_event(defines.events.on_entity_died, function(event) event_on_entity_died(event) end)

script.on_event(defines.events.on_player_rotated_entity, function(event) event_on_player_rotated_entity(event) end)

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
script.on_init(on_init)
script.on_load(on_Load)
script.on_configuration_changed(on_update)