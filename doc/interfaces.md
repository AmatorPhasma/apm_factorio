# Remote interfaces in the data and control stage
Version 0.19.0 (online version: [https://gitlab.com/AmatorPhasma/apm_factorio/blob/master/doc/interfaces.md](https://gitlab.com/AmatorPhasma/apm_factorio/blob/master/doc/interfaces.md))

## __MOD: Amator Phasma's Library__
__Data stage__:

Define an item as radioactiv in the data stage generates the descripton for it.<br/>
*usage in any data stage type after this library's data stage with:<br/>
`apm.lib.utils.item.add.radioactive_description(item_name :: string, level :: int)`<br/>
level: (optional / default is 1)
- 1: low radioactive
- 2: medium radioactive
- 3: high radioactive

Examples:
```lua
apm.lib.utils.item.add.radioactive_description('apm_yellowcake')
apm.lib.utils.item.add.radioactive_description('apm_yellowcake', 1)
```

![](https://gitlab.com/AmatorPhasma/apm_factorio/raw/stable-0.18/doc/radioactive_tooltip.png)
___
__Control stage__:
Define an item as radioactiv in the control stage by using a remote interface to register or unregister it to give the player damge if he has one in his inventory.

Register an item:<br/>
`remote.call('apm_radiation', 'add_item', item_name :: string, level :: int)`<br/>
level: (optional / default is 1)
- 1: low radioactive
- 2: medium radioactive
- 3: high radioactive

Unregister an items:<br/>
`remote.call('apm_radiation', 'remove_item', item_name :: string)`

List all registered items:<br/>
`remote.call('apm_radiation', 'list_items')` -> returns a table

Examples:
```lua
remote.call('apm_radiation', 'add_item', 'uranium-ore', 1)
remote.call('apm_radiation', 'remove_item', 'uranium-ore')
remote.call('apm_radiation', 'list_items')
```

Define an equipment with the burner property.

Register an equipment:<br/>
`remote.call('apm_equipment', 'add_burner_equipment', string :: LuaGeneratorEquipment.name)`<br/>

Unregister an items:<br/>
`remote.call('apm_equipment', 'del_burner_equipment', string :: LuaGeneratorEquipment.name)`<br/>

Examples:
```lua
remote.call('apm_equipment', 'add_burner_equipment', 'fusion-reactor-equipment')
remote.call('apm_equipment', 'del_burner_equipment', 'fusion-reactor-equipment')
```

Force a check if an player has equipped a registered burner equipment and can toggle the equipment manager:<br/>
`remote.call('apm_equipment', 'check_equipment_manager', player :: LuaPlayer)`<br/>

You can also register to the following events:
they return `event{player = LuaPlayer, LuqEquipment = equipment}`
- event_on_burner_equipment_low_fuel
```lua
script.on_event(remote.call("apm_equipment", "event_on_burner_equipment_low_fuel"), function(event)
    -- your stuff here
end)
```
- event_on_burner_equipment_burnt_result_inventory_full
```lua
script.on_event(remote.call("apm_equipment", "event_on_burner_equipment_burnt_result_inventory_full"), function(event)
    -- your stuff here
end)
```
WARNING: this has to be done within on_init and on_load, otherwise the game will error about the remote.call

## __MOD: Amator Phasma's Recycling__
__Data stage__:

You can easily add scrap metal to products in the data-updates phase with:<br/>
`apm.lib.utils.recycling.scrap.add(recipe=LuaRecipe.name, metal=Type/String, probability=Type/Double)`<br/>
-- The `probability` is optional to overwrite the value from the settings

Example:
```lua
-- This results in a 10% probability for iron scrap
apm.lib.utils.recycling.scrap.add({recipe='pipe', metal='iron'})

-- This results in a 5% probability for iron scrap and 5% for copper scrap
apm.lib.utils.recycling.scrap.add({recipe='pipe', metal='iron'})
apm.lib.utils.recycling.scrap.add({recipe='pipe', metal='copper'})

-- This results in a 30% probability for iron scrap
apm.lib.utils.recycling.scrap.add({recipe='pipe', metal='iron', probability=0.3})
```

Delete the scrap metal output generation for a recipe with:<br/>
`apm.lib.utils.recycling.scrap.remove({recipe=LuaRecipe.name})`

Example:
```lua
apm.lib.utils.recycling.scrap.remove({recipe='pipe'})
```

To define your one scrap metal you need a bit more:<br/>
`apm.lib.utils.recycling.metal.add(options=Type/Table)`<br/>
* name :: string -> an arbitrary name for a metal (like: iron, copper, titanium, etc..)<br/>
* tint :: Color -> the color of this metal<br/>
* output :: LuaItem.name -> an prototype name of an item<br/>
* output_category: -> the recipe-category for the last recycling step (Default: 'smelting' example: for angel metals: 'induction-smelting')<br/>
* wight: -> int: <4 for tier 1, =4 tier 1 recycler but tier 2 technologie, <8 for tier 2, <11 for tier 3, >=11 for tier 4. In the future this will also determine the importance of this metal for a further automatic generation.<br/>
* output_probability :: nil a dummy for the futere, has no impact at this moment<br/>
* t_catalysts :: LuaTable -> example: {{type='fluid', name='steam', amount=100}} (*only two inpute-pipe)<br/>
* t_output_byproducts :: LuaTable -> example: {{type='fluid', name='water', amount=50}} (*only two output-pipe)<br/>
* b_own_tech :: Bool -> if true there is a own tech generated for the recipes of this metal/scrap process<br/>
* t_tech_prerequisites :: LuaTable -> has only effect if b_own_tech is true, a table of technologies that are used as prerequisites for this recipe chain<br/>
* output_amount_overwrite :: int -> overwrites the default output for the last recipe in the process<br/>

Example:
```lua
apm.lib.utils.recycling.metal.add({name='iron', tint={r= 0.56, g = 0.63, b = 0.74}, output='iron-plate', wight=1})
apm.lib.utils.recycling.metal.add({name='tin', tint={r= 0.33, g = 0.49, b = 0.36}, output='liquid-molten-tin', output_category='induction-smelting', wight=2, b_own_tech=true, t_tech_prerequisites={'angels-tin-smelting-1'}})
apm.lib.utils.recycling.metal.add({name='lead', tint={r= 0.60, g = 0.75, b = 0.90}, output='lead-plate', wight=2, b_own_tech=true})
```

If you add an Metal thats not defines in my [./prototypes/recycling/generate_metal.lua](https://gitlab.com/AmatorPhasma/apm_factorio/blob/master/apm_recycling/prototypes/recycling/generate_metal.lua) you need to add an local string for translations:<br/>
Example:
```cfg
apm_metal_type_iron=iron
apm_metal_type_copper=Kupfer
```

If you want look in the code: [you can it doe here...](https://gitlab.com/AmatorPhasma/apm_factorio/blob/master/apm_lib/lib/utils/recycling.lua)

__Important:__ You have to create the metal type BEFORE you can add scrap metal to a recipe, this means you should prepare the load order to load your data-updates phase after this mod, to use the "defaults" defined in ./prototypes/recycling/generate.lua

## __MOD: Amator Phasma's Starfall__
__Control stage__:

Register a surface:<br/>
`remote.call('apm_starfall', 'add_surface', surface :: LuaSurface.index)`

Unregister a surface:<br/>
`remote.call('apm_starfall', 'remove_surface', surface :: LuaSurface.index)`

List registered surfaces:<br/>
`remote.call('apm_starfall', 'list_surfaces')` -> returns a table

Queue up an event for a surface:<br/>
`remote.call('apm_starfall', 'add_event', surface :: LuaSurface.index, amount :: int, ticks :: Game.Ticks, range :: LuaArea)`

Examples:
```lua
remote.call('apm_starfall', 'add_surface', 1)
remote.call('apm_starfall', 'remove_surface', 1)
remote.call('apm_starfall', 'list_surfaces')
remote.call('apm_starfall', 'add_event', 1, 2, game.tick+600)
remote.call('apm_starfall', 'add_event', 1, 2, game.tick+600, {max=128,min=10})
```