require('__apm_lib__.lib.log')

local self = 'apm_angel_addon/lib/functions.lua'

APM_LOG_HEADER(self)

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
function apm.angel_addon.casting_machine.update(entity_name)
    if not apm.lib.utils.assembler.exist(entity_name) then return end

    local output_fluidbox = {}
    output_fluidbox.production_type = "output"
    output_fluidbox.pipe_covers = pipecoverspictures()
    output_fluidbox.base_area = 10
    output_fluidbox.base_level = 1
    output_fluidbox.pipe_connections = {{ position = {2, -1} }}
    local casting_machine = data.raw['assembling-machine'][entity_name]

    table.insert(casting_machine.fluid_boxes, output_fluidbox)

    APM_LOG_INFO(self, 'apm.angel_addon.casting_machine.update()', '"' ..tostring(entity_name).. '" updated.')
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function count_ingriedent_fluids(recipe)
    local s_count = 0

    if recipe.ingredients then
        for _, _ in pairs(recipe.ingredients) do
            s_count = s_count + 1
        end
    elseif recipe.normal then
        for _, _ in pairs(recipe.normal.ingredients) do
            s_count = s_count + 1
        end
    end

    return s_count
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
function apm.angel_addon.recipes.add_purified_water_to_casting_recipes()
    for _, recipe in pairs(data.raw.recipe) do
        if recipe.category == 'casting' then
            if count_ingriedent_fluids(recipe) <= 2 then
                apm.lib.utils.recipe.ingredient.mod(recipe.name, 'water-purified', 20)
                apm.lib.utils.recipe.result.mod(recipe.name, 'steam', 40)
                apm.lib.utils.recipe.result.mod_temperature(recipe.name, 'steam', nil, 120)
                apm.lib.utils.recipe.add_mainproduct_if_needed(recipe.name, true)
                APM_LOG_INFO(self, 'add_purified_water_to_casting_recipes()', 'recipe: "' ..tostring(recipe.name).. '" added input: "water-purified" and output: "steam"')
            end
        end
    end
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
function apm.angel_addon.recipes.add_steam_to_strand_casting_recipes()
    for _, recipe in pairs(data.raw.recipe) do
        if recipe.category == 'strand-casting' then
            if apm.lib.utils.recipe.has.ingredient(recipe.name, 'water') and
                    not apm.lib.utils.recipe.has.ingredient(recipe.name, 'liquid-coolant') then
                apm.lib.utils.recipe.result.mod(recipe.name, 'steam', 80)
                apm.lib.utils.recipe.result.mod_temperature(recipe.name, 'steam', nil, 120)
                apm.lib.utils.recipe.add_mainproduct_if_needed(recipe.name, true)
                APM_LOG_INFO(self, 'add_steam_to_sintering_recipes()', 'recipe: "' ..tostring(recipe.name).. '" added output: "steam"')
            end
        end
    end
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
function apm.angel_addon.recipes.create(item_name, amount, metal_name, ingredients_base_amount, catalyst_item, category, subgroup, parent_technology_name, science_amount)
    if not apm.lib.utils.item.exist(item_name) then return end

    local input
    if category == 'casting' then
        input = 'liquid-molten-'..metal_name
    elseif category == 'sintering' then
        input = 'powder-'..metal_name
        if not apm.lib.utils.item.exist(input) then
            input = 'casting-powder-'..metal_name
        end
    else
        APM_LOG_ERR(self,'recipes.create','category: must be "casting" or "sintering"')
        return
    end

    if not apm.lib.utils.item.exist(input) then
        APM_LOG_ERR(self,'recipes.create','liquid ore powder: "' ..tostring(input).. '" does not exist')
        return
    end

    local ingredient_multi
    local energy_required
    if category == 'casting' then
        ingredient_multi = 0.85
        energy_required = 3.5
    elseif category == 'sintering' then
        ingredient_multi = 0.75
        energy_required = 4.5
    end

    local type_name = apm.lib.utils.item.get_type(input)
    local ingredient_amount = amount * ingredients_base_amount * ingredient_multi
    if type_name == 'item' then
        ingredient_amount = apm.lib.utils.math.round(ingredient_amount, 0)
    end

    -- recipe
    -- ---------------------------------------------------------------------------------------------
    local item_icon_a = apm.lib.utils.icon.get.from_item(item_name)
    local icons = apm.lib.utils.icon.merge({item_icon_a})

    local recipe = {}
    recipe.type = 'recipe'
    recipe.name = 'apm_' ..category.. '_of_' .. item_name
    recipe.localised_name = {"item-name." ..tostring(item_name)}
    recipe.category = category
    recipe.icons = icons
    --recipe.group = ''
    recipe.subgroup = subgroup or 'angels-'..tostring(metal_name)..'-casting'
    recipe.order = 'ea_a'
    recipe.normal = {}
    recipe.normal.enabled = false
    recipe.normal.energy_required = energy_required
    -- normal
    type_name = apm.lib.utils.item.get_type(input)

    recipe.normal.ingredients = {
        {type=type_name, name=input, amount=ingredient_amount},
        {type='item', name=catalyst_item, amount=1}
    }
    type_name = apm.lib.utils.item.get_type(item_name)
    recipe.normal.results = {
        {type=type_name, name=item_name, amount=amount},
        {type='item', name=catalyst_item, amount_min=1, amount_max=1, probability=0.90, catalyst_amount=1}
    }
    recipe.normal.always_show_products = true
    recipe.normal.always_show_made_in = true
    -- expensive
    recipe.expensive = table.deepcopy(recipe.normal)
    recipe.expensive.energy_required = 3
    type_name = apm.lib.utils.item.get_type(input)

    recipe.expensive.ingredients = {
        {type=type_name, name=input, amount=ingredient_amount},
        {type='item', name=catalyst_item, amount=1}
    }
    type_name = apm.lib.utils.item.get_type(item_name)
    recipe.expensive.results = {
        {type=type_name, name=item_name, amount=amount},
        {type='item', name=catalyst_item, amount_min=1, amount_max=1, probability=0.80, catalyst_amount=1}
    }
    data:extend({recipe})

    if mods.apm_recycling and category == 'casting' then
        apm.lib.utils.recycling.scrap.add(recipe.name, metal_name)
    end

    -- technologies
    -- ---------------------------------------------------------------------------------------------
    local tech_name = 'apm_' ..category.. '_' ..item_name
    if not apm.lib.utils.technology.exist(tech_name) then
        local tech = {}
        tech.type = 'technology'
        tech.name = tech_name
        tech.localised_name = {"technology-name.apm_angel_addon_recipe_" ..category, {'item-name.' ..item_name}}

        local item_icon_a = {{icon = '__apm_resource_pack__/graphics/technologies/dynamics/'..catalyst_item..'.png', icon_size=128}}
        local item_icon_b = apm.lib.utils.icon.get.from_item(input)
        item_icon_b = apm.lib.utils.icons.mod(item_icon_b, 1.5, {42, -25})
        local item_icon_c = apm.lib.utils.icon.get.from_item(item_name)
        item_icon_c = apm.lib.utils.icons.mod(item_icon_c, 1.5, {42, 25})
        local icons = apm.lib.utils.icon.merge({item_icon_a, item_icon_b, item_icon_c})

        tech.icons = icons
        --tech.icon_size = 128

        tech.effects = {
            {type = 'unlock-recipe', recipe = recipe.name}
        }
        tech.prerequisites = {'apm_casting_tools'}

        if parent_technology_name == nil then
            parent_technology_name = 'angels-' .. metal_name .. '-smelting-1'
        end
        table.insert(tech.prerequisites, parent_technology_name)

        if category == 'sintering' then
            table.insert(tech.prerequisites, 'powder-metallurgy-1')
            table.insert(tech.prerequisites, 'production-science-pack')
        end

        tech.unit = {}
        if science_amount == nil then
            science_amount = 50
        end
        tech.unit.count = science_amount
        tech.unit.ingredients = {{"automation-science-pack", 1}}
        tech.unit.time = 30
        tech.order = 'aa_' ..tech_name
        data:extend({tech})
        apm.lib.utils.technology.set.heritage_science_packs_from_prerequisites(tech.name)
        if category == 'sintering' then
            apm.lib.utils.technology.add.science_pack(tech_name, 'production-science-pack', 1)
        end
    else
        apm.lib.utils.technology.force.recipe_for_unlock(tech_name, recipe.name)
    end
end
