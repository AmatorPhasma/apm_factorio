__Amator Phasma's Angel (Addons) [Alpha]__
=============
[Source Code on GitLab](https://gitlab.com/AmatorPhasma/apm_factorio)

__Languages:__
-------------
English, Deutsch

__About this mod:__
-------------
__Be aware: This mod is not finished and will face heavy changes in the future.__

This mod contains all my independent changes to Angel's mods.

__All the following changes can be configured in the settings, the default for all these is activated:__
- Changed all casting recipes to need purified water (20) as an additional input, and steam (40) as an additional output.
- Adds new sintering and/or casting recipes for gears, bearing balls, etc.
- Overhaul: steam conversion recipes", this overhaul option unifies steam from liquids and steam to liquids to a ratio of: 1:2 (liquid <-> steam)
- Overhaul: strand casting recipes", adds steam (80) as an additional output for strand casting machine recipes that need water.
- See floor pumps need electricity
- Show always made in information on recipe tooltips

__Future?__
-------------
- More changes to casting, sintering, etc...
- Add additional tools/catalys production cycles.

__Problems?__
-------------
- Report them on the forum thread or here on this site under disscussion.

__Ideas?__
-------------
- Please tell me on the forum thread.

__Dependencies:__
-------------
- None

__Known incompatibility:__
-------------
- None

__My other mods:__
-------------
- [Amator Phasma's Library](https://mods.factorio.com/mod/apm_lib)
- [Amator Phasma's Modpack](https://mods.factorio.com/mod/apm_modpack)
- [Amator Phasma's Coal&Steam](https://mods.factorio.com/mod/apm_power)
- [Amator Phasma's Nuclear](https://mods.factorio.com/mod/apm_nuclear)
- [Amator Phasma's Recycling](https://mods.factorio.com/mod/apm_recycling)
- [Amator Phasma's Starfall](https://mods.factorio.com/mod/apm_starfall)
- [Amator Phasma's Energy Addon](https://mods.factorio.com/mod/apm_energy_addon)

[My thread in the forum.](https://forums.factorio.com/viewtopic.php?f=190&t=68748)