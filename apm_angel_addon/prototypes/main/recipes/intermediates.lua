require('util')
require('__apm_lib__.lib.log')

local self = 'apm_angel_Addon/prototypes/main/recipes/intermediates.lua'

APM_LOG_HEADER(self)

local apm_angel_addon_always_show_made_in = settings.startup["apm_angel_addon_always_show_made_in"].value
local apm_angel_addon_casting_recipes = settings.startup["apm_angel_addon_casting_recipes"].value

APM_LOG_SETTINGS(self, 'apm_angel_addon_always_show_made_in', apm_angel_addon_always_show_made_in)
APM_LOG_SETTINGS(self, 'apm_angel_addon_casting_recipes', apm_angel_addon_casting_recipes)

if not apm_angel_addon_casting_recipes then return end

-- Recipe ---------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local recipe = {}
recipe.type = "recipe"
recipe.name = "apm_mold_gears"
recipe.category = 'advanced-crafting'
recipe.normal = {}
recipe.normal.enabled = false
recipe.normal.energy_required = 2
recipe.normal.ingredients = {
        apm.lib.utils.builder.recipe.item.simple('APM_MOLD_BASE', 4)
    }
recipe.normal.results = { 
        {type='item', name='apm_mold_gears', amount=1}
    }
recipe.normal.main_product = 'apm_mold_gears'
recipe.normal.requester_paste_multiplier = 4
recipe.normal.always_show_products = true
recipe.normal.always_show_made_in = apm_angel_addon_always_show_made_in
--recipe.normal.allow_decomposition = false
--recipe.normal.allow_as_intermediate = false
--recipe.normal.allow_intermediates = false
recipe.expensive = table.deepcopy(recipe.normal)
--recipe.expensive.energy_required =
--recipe.expensive.ingredients = {}
--recipe.expensive.results = {}
data:extend({recipe})

-- Recipe ---------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local recipe = {}
recipe.type = "recipe"
recipe.name = "apm_mold_balls"
recipe.category = 'advanced-crafting'
recipe.normal = {}
recipe.normal.enabled = false
recipe.normal.energy_required = 2
recipe.normal.ingredients = {
        apm.lib.utils.builder.recipe.item.simple('APM_MOLD_BASE', 4)
    }
recipe.normal.results = {
        {type='item', name='apm_mold_balls', amount=1}
    }
recipe.normal.main_product = 'apm_mold_balls'
recipe.normal.requester_paste_multiplier = 4
recipe.normal.always_show_products = true
recipe.normal.always_show_made_in = apm_angel_addon_always_show_made_in
--recipe.normal.allow_decomposition = false
--recipe.normal.allow_as_intermediate = false
--recipe.normal.allow_intermediates = false
recipe.expensive = table.deepcopy(recipe.normal)
--recipe.expensive.energy_required =
--recipe.expensive.ingredients = {}
--recipe.expensive.results = {}
data:extend({recipe})