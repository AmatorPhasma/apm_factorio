require ('util')
require('__apm_lib__.lib.log')

local self = 'apm_angel_addon/prototypes/integrations/recipes.lua'

APM_LOG_HEADER(self)

local apm_angel_addon_casting_machine_recipes = settings.startup["apm_angel_addon_casting_machine_recipes"].value
local apm_angel_addon_strand_casting_machine_recipes = settings.startup["apm_angel_addon_strand_casting_machine_recipes"].value
local apm_angel_addon_casting_recipes = settings.startup["apm_angel_addon_casting_recipes"].value
local apm_angel_addon_always_show_made_in = settings.startup["apm_angel_addon_always_show_made_in"].value
local apm_angel_addon_steam_unification = settings.startup["apm_angel_addon_steam_unification"].value
local apm_angel_addon_compat_bob = settings.startup["apm_angel_addon_compat_bob"].value
local apm_angel_addon_compat_shiny_angel_gfx = settings.startup["apm_angel_addon_compat_shiny_angel_gfx"].value
local apm_angel_addon_electric_seafloor_pump = settings.startup["apm_angel_addon_electric_seafloor_pump"].value

APM_LOG_SETTINGS(self, 'apm_angel_addon_casting_machine_recipes', apm_angel_addon_casting_machine_recipes)
APM_LOG_SETTINGS(self, 'apm_angel_addon_strand_casting_machine_recipes', apm_angel_addon_strand_casting_machine_recipes)
APM_LOG_SETTINGS(self, 'apm_angel_addon_casting_recipes', apm_angel_addon_casting_recipes)
APM_LOG_SETTINGS(self, 'apm_angel_addon_always_show_made_in', apm_angel_addon_always_show_made_in)
APM_LOG_SETTINGS(self, 'apm_angel_addon_steam_unification', apm_angel_addon_steam_unification)
APM_LOG_SETTINGS(self, 'apm_angel_addon_compat_bob', apm_angel_addon_compat_bob)
APM_LOG_SETTINGS(self, 'apm_angel_addon_compat_shiny_angel_gfx', apm_angel_addon_compat_shiny_angel_gfx)
APM_LOG_SETTINGS(self, 'apm_angel_addon_electric_seafloor_pump', apm_angel_addon_electric_seafloor_pump)

local iron_gear_base = 2
if mods.apm_power then
    iron_gear_base = 1
end

if apm_angel_addon_casting_recipes then
    if mods.apm_power then
        apm.lib.utils.recipe.category.change('apm_mold_gears', 'apm_press')
        apm.lib.utils.recipe.category.change('apm_mold_balls', 'apm_press')
    end
    apm.angel_addon.recipes.create('iron-gear-wheel', 8, 'iron', iron_gear_base*10, 'apm_mold_gears', 'casting', nil, nil, 75)
    apm.angel_addon.recipes.create('iron-gear-wheel', 8, 'iron', iron_gear_base, 'apm_mold_gears', 'sintering', nil, nil, 75)
end

-- bob ------------------------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------------------
if mods.bobplates and apm_angel_addon_compat_bob then
    if apm_angel_addon_casting_recipes then
        -- gear-wheels
        apm.angel_addon.recipes.create('steel-gear-wheel', 8, 'steel', 10, 'apm_mold_gears', 'casting', nil, nil, 75)
        apm.angel_addon.recipes.create('steel-gear-wheel', 8, 'steel', 1, 'apm_mold_gears', 'sintering', nil, nil, 75)
        apm.angel_addon.recipes.create('cobalt-steel-gear-wheel', 8, 'cobalt-steel', 10, 'apm_mold_gears', 'casting', 'angels-alloys-casting', nil, 75)
        apm.angel_addon.recipes.create('brass-gear-wheel', 8, 'brass', 10, 'apm_mold_gears', 'casting', 'angels-alloys-casting', nil, 75)
        apm.angel_addon.recipes.create('titanium-gear-wheel', 8, 'titanium', 10, 'apm_mold_gears', 'casting', nil,nil, 75)
        apm.angel_addon.recipes.create('nitinol-gear-wheel', 8, 'nitinol',10, 'apm_mold_gears', 'casting', 'angels-alloys-casting', nil, 75)
        apm.angel_addon.recipes.create('tungsten-gear-wheel', 8, 'tungsten', 1, 'apm_mold_gears', 'sintering', nil, nil, 75)
        -- bearing-balls
        apm.angel_addon.recipes.create('steel-bearing-ball', 36, 'steel', 10/12, 'apm_mold_balls', 'casting', nil, nil, 75)
        apm.angel_addon.recipes.create('steel-bearing-ball', 36, 'steel', 1/12, 'apm_mold_balls', 'sintering', nil, nil, 75)
        apm.angel_addon.recipes.create('cobalt-steel-bearing-ball', 36, 'cobalt-steel', 30/36, 'apm_mold_balls', 'casting', 'angels-alloys-casting', nil, 75)
        apm.angel_addon.recipes.create('nitinol-bearing-ball', 36, 'nitinol', 10/12, 'apm_mold_balls', 'casting', 'angels-alloys-casting', nil, 75)
        apm.angel_addon.recipes.create('titanium-bearing-ball', 36, 'titanium', 10/12, 'apm_mold_balls', 'casting', nil, nil, 75)
    end
end

-- ----------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------

if apm_angel_addon_casting_machine_recipes then
    apm.angel_addon.recipes.add_purified_water_to_casting_recipes()
end

if apm_angel_addon_strand_casting_machine_recipes then
    apm.angel_addon.recipes.add_steam_to_strand_casting_recipes()
end

if apm_angel_addon_steam_unification then
    if not mods.apm_power and not mods.apm_nuclear then
        apm.lib.utils.recipe.result.mod('coolant-cool-steam', 'water-purified', 45)
		apm.lib.utils.recipe.result.mod('coolant-cool-100', 'steam', 100)
		apm.lib.utils.recipe.result.mod('coolant-cool-200', 'steam', 100)
		apm.lib.utils.recipe.result.mod('coolant-cool-300', 'steam', 100)
    end
end

if apm_angel_addon_always_show_made_in then
    local recipe_categories = {
        ['ore-processing'] = true,
        ['pellet-pressing'] = true,
        ['powder-mixing'] = true,
        ['blast-smelting'] = true,
        ['chemical-smelting'] = true,
        ['induction-smelting'] = true,
        ['casting'] = true,
        ['strand-casting'] = true,
        ['sintering'] = true,
        ['cooling'] = true,
        ['ore-processing'] = true,
        ['ore-sorting'] = true,
        ['ore-sorting-t1'] = true,
        ['ore-sorting-t1-5'] = true,
        ['ore-sorting-t2'] = true,
        ['ore-sorting-t3'] = true,
        ['ore-sorting-t3-5'] = true,
        ['ore-sorting-t4'] = true,
        ['liquifying'] = true,
        ['filtering'] = true,
        ['crystallizing'] = true,
        ['angels-barreling'] = true,
        ['water-treatment'] = true,
        ['salination-plant'] = true,
        ['washing-plant'] = true,
        ['barreling-pump'] = true,
        ['angels-water-void'] = true,
        ['gas-refining'] = true,
        ['steam-cracking'] = true,
        ['petrochem-separation'] = true,
        ['advanced-chemistry'] = true,
        ['angels-converter'] = true,
        ['petrochem-electrolyser'] = true,
        ['petrochem-air-filtering'] = true,
        ['angels-chemical-void'] = true,
    }

    for _, recipe in pairs(data.raw.recipe) do
        if recipe_categories[recipe.category] then
            apm.lib.utils.recipe.set.always_show_products(recipe.name, true)
            apm.lib.utils.recipe.set.always_show_made_in(recipe.name, true)
        end
    end
end
