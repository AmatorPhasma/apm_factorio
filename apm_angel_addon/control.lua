-- Requires Defines------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local updates = require('lib.updates')
local seafloor_pumps = require('lib.seafloor_pumps')

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function on_init()
    updates.run()
    seafloor_pumps.on_init()
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function on_load()
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function on_update()
    updates.run()
    seafloor_pumps.on_update()
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function event_on_build(event)
    if event.created_entity.valid ~= true then return end
    seafloor_pumps.on_build(event.created_entity)
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function event_on_robot_build(event)
    if event.created_entity.valid ~= true then return end
    seafloor_pumps.on_build(event.created_entity)
end

-- Event Defines---------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local entity_build_filter = {
    {filter='type', type='offshore-pump'}
}

script.on_event(defines.events.on_built_entity, function(event) event_on_build(event) end, entity_build_filter)
script.on_event(defines.events.on_robot_built_entity, function(event) event_on_robot_build(event) end, entity_build_filter)

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
script.on_init(on_init)
script.on_load(on_load)
script.on_configuration_changed(on_update)