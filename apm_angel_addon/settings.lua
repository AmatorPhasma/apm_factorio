-- startup --------------------------------------------------------------------
--
--
-------------------------------------------------------------------------------
data:extend(
{
  {type = "bool-setting", name = "apm_angel_addon_casting_machine_recipes", setting_type = "startup", default_value = true, order='aa_a'},
  {type = "bool-setting", name = "apm_angel_addon_strand_casting_machine_recipes", setting_type = "startup", default_value = true, order='aa_b'},
  {type = "bool-setting", name = "apm_angel_addon_casting_recipes", setting_type = "startup", default_value = true, order='ab_a'},
  {type = "bool-setting", name = "apm_angel_addon_electric_seafloor_pump", setting_type = "startup", default_value = true, order='ac_a'},
  {type = "bool-setting", name = "apm_angel_addon_steam_unification", setting_type = "startup", default_value = true, order='ad_a'},

  {type = "bool-setting", name = "apm_angel_addon_always_show_made_in", setting_type = "startup", default_value = true, order='ba_a'},

  {type = "bool-setting", name = "apm_angel_addon_compat_bob", setting_type = "startup", default_value = true, order='pa_a'},
  {type = "bool-setting", name = "apm_angel_addon_compat_shiny_angel_gfx", setting_type = "startup", default_value = true, order='pb_a'},
})