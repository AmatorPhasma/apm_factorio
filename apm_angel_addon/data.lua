if apm == nil then apm = {} end
if apm.angel_addon == nil then apm.angel_addon = {} end

require('lib.definitions')
require('lib.functions')

require('prototypes.main.categories')

require('prototypes.main.items.intermediates')
require('prototypes.main.recipes.intermediates')
require('prototypes.main.technologies')
