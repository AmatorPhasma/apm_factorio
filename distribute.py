#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

# Factorio mod distribution script.
# Copyright (C) 2019 Amator Phasma
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import sys
from glob import glob
from distutils.dir_util import copy_tree

from python import Factorio, excepthook

__title__ = 'Distribution script'
__author__ = 'AmatorPhasma (Bjoern Trimborn)'
__email__ = 'factorio@bjoern-trimborn.de'
__date__ = '2019.12.08'
__license__ = 'GPL v3'
__status__ = 'Stable'


class Distribute(object):
    def __init__(self) -> None:
        self.__factorio = Factorio(factorio_path='F:/Factorio')

    def __copy(self, mod_name) -> None:
        mod_version = self.__factorio.mod_version(mod_name)
        factorio_mod_path = '{}/mods/{}_{}'.format(self.__factorio.factorio_path, mod_name, mod_version)
        print('copy mod: {}, version: {}, into factorio mod directory'.format(mod_name, mod_version))
        copy_tree(src='./{}/'.format(mod_name), dst=factorio_mod_path, update=True)

    def specific(self, mod_name: str) -> None:
        self.__copy(mod_name=mod_name)

    def run(self) -> None:
        modlist = glob('./apm_*')
        for modpath in modlist:
            mod_name = str(modpath).lstrip('.').lstrip('\\').lstrip('/')
            self.__copy(mod_name=mod_name)


if __name__ == '__main__':
    sys.excepthook = excepthook
    args = sys.argv
    dist = Distribute()

    if len(args) < 2:
        dist.run()
    elif len(args) >= 2:
        for mod in args[1:]:
            dist.specific(mod_name=mod)
