#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

# Factorio mod release script.
# Copyright (C) 2019 Amator Phasma
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import os
import sys
from glob import glob
from python import Factorio, excepthook
from zipfile import ZipFile

__title__ = 'Release script'
__author__ = 'AmatorPhasma (Bjoern Trimborn)'
__email__ = 'factorio@bjoern-trimborn.de'
__date__ = '2019.12.08'
__license__ = 'GPL v3'
__status__ = 'Stable'


class Release(object):
    def __init__(self, overwrite: bool = False) -> None:
        self.__factorio = Factorio()
        self.__overwrite = overwrite

    def __build(self, mod_name) -> None:
        mod_version = self.__factorio.mod_version(mod_name)
        mod_src = './{}/'.format(mod_name)
        mod_dir = '{}_{}'.format(mod_name, mod_version)
        mod_zip = './release/{}.zip'.format(mod_dir)

        if os.path.isfile(mod_zip) and not self.__overwrite:
            print('WARNING: mod: {}, version: {}, already released'.format(mod_name, mod_version))
            return

        with ZipFile(mod_zip, 'w') as zfile:
            for folder_name, sub_folders, file_names in os.walk(mod_src):
                for filen_ame in file_names:
                    # create complete filepath of file in directory
                    file_path = os.path.join(folder_name, filen_ame)
                    # Add file to zip
                    zfile.write(file_path)

        print('INFO: mod: {}, version: {}, released'.format(mod_name, mod_version))

    def specific(self, mod_name: str) -> None:
        self.__build(mod_name=mod_name)

    def run(self) -> None:
        modlist = glob('./apm_*')
        for modpath in modlist:
            mod_name = str(modpath).lstrip('.').lstrip('\\').lstrip('/')
            self.__build(mod_name=mod_name)


if __name__ == '__main__':
    sys.excepthook = excepthook
    args = sys.argv
    rel = Release()

    if len(args) < 2:
        rel.run()
    elif len(args) >= 2:
        for mod in args[1:]:
            rel.specific(mod_name=mod)
