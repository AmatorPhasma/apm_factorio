#!/usr/bin/env python3.8
# -*- coding: utf-8 -*-

# Copyright (C) 2019 Amator Phasma
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import sys
import time
import traceback
from io import StringIO


ERR_TO_STDERR = True


def excepthook(exctype, value, tracebackobj):
    """
    Global function to catch unhandled exceptions.

    @param exctype exception type
    @param value exception value
    @param tracebackobj traceback object
    """
    separator = '-' * 80
    time_string = time.strftime("%Y-%m-%d, %H:%M:%S")
    tbinfofile = StringIO()
    traceback.print_tb(tracebackobj, None, tbinfofile)
    tbinfofile.seek(0)
    tbinfo = tbinfofile.read()
    errmsg = '{}: \n{}'.format(str(exctype), str(value))
    sections = [separator, time_string, separator, errmsg, separator, tbinfo]
    msg = '\n'.join(sections)

    if ERR_TO_STDERR:
        print(str(msg) + str('\n'), file=sys.stderr)
