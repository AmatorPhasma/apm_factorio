require('util')

apm.lib.utils.recipe.category.create('apm_gas_vent')

-- Item -----------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local item = {}
item.type = 'item'
item.name = 'apm_gas_vent'
item.icons = {
    apm.lib.icons.dummy
}
item.stack_size = 2000
item.group = "other"
item.subgroup = "other"
item.order = 'aa_a'
item.place_result = "apm_gas_vent"
data:extend({item})

-- Entity ---------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local animation = {}
animation.north = {}
animation.north.layers = {}
animation.north.layers[1] = {}
animation.north.layers[1].filename = "__apm_dummy__/pipe-straight-vertical.png"
animation.north.layers[1].priority="high"
animation.north.layers[1].width = 64
animation.north.layers[1].height = 64
animation.north.layers[1].frame_count = 1
animation.north.layers[1].line_length = 1
--animation.north.layers[1].shift = {1.21875, -1.21875}
animation.north.layers[1].animation_speed = 1
animation.north.layers[1].hr_version = {}
animation.north.layers[1].hr_version.filename = "__apm_dummy__/hr-pipe-straight-vertical.png"
animation.north.layers[1].hr_version.priority="high"
animation.north.layers[1].hr_version.width = 128
animation.north.layers[1].hr_version.height = 128
animation.north.layers[1].hr_version.frame_count = 1
animation.north.layers[1].hr_version.line_length = 1
--animation.north.layers[1].hr_version.shift = {1.21875, -1.21875}
animation.north.layers[1].hr_version.scale = 0.5
animation.north.layers[1].hr_version.animation_speed = 1
animation.east = table.deepcopy(animation.north)
animation.east.layers[1].filename = "__apm_dummy__/pipe-straight-horizontal.png"
animation.east.layers[1].hr_version.filename = "__apm_dummy__/hr-pipe-straight-horizontal.png"
animation.south = table.deepcopy(animation.north)
animation.west = table.deepcopy(animation.east)

-- Entity ---------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local hybrid_cooling_tower = {}
hybrid_cooling_tower.type = "furnace"
hybrid_cooling_tower.name = "apm_gas_vent"
hybrid_cooling_tower.icons = {
    apm.lib.icons.dummy
}
hybrid_cooling_tower.icon_size = 32
hybrid_cooling_tower.flags = {"placeable-neutral", "placeable-player", "player-creation"}
hybrid_cooling_tower.minable = {mining_time = 0.2, result = "apm_gas_vent"}
hybrid_cooling_tower.crafting_categories = {"apm_gas_vent"}
hybrid_cooling_tower.crafting_speed = 1
hybrid_cooling_tower.source_inventory_size = 0
hybrid_cooling_tower.result_inventory_size = 0
--hybrid_cooling_tower.fast_replaceable_group = "apm_hybrid_cooling_tower"
--hybrid_cooling_tower.next_upgrade = nil
hybrid_cooling_tower.max_health = 100
hybrid_cooling_tower.corpse = "big-remnants"
hybrid_cooling_tower.dying_explosion = "medium-explosion"
hybrid_cooling_tower.resistances = {{type = "fire", percent = 90}}
hybrid_cooling_tower.collision_box = {{-0.4, -0.4}, {0.4, 0.4}}
hybrid_cooling_tower.selection_box = {{-0.5, -0.5}, {0.5, 0.5}}
hybrid_cooling_tower.light = {intensity = 0.6, size = 9.9, shift = {0.0, 0.0}, color = {r = 1.0, g = 0.5, b = 0.0}}
hybrid_cooling_tower.open_sound = { filename = "__base__/sound/machine-open.ogg", volume = 0.85 }
hybrid_cooling_tower.close_sound = { filename = "__base__/sound/machine-close.ogg", volume = 0.75 }
hybrid_cooling_tower.vehicle_impact_sound =  { filename = "__base__/sound/car-metal-impact.ogg", volume = 0.65 }
hybrid_cooling_tower.working_sound = {}
hybrid_cooling_tower.working_sound.sound = {}
hybrid_cooling_tower.working_sound.sound.filename = "__apm_resource_pack__/sounds/entities/hybrid_cooling_tower.ogg"
hybrid_cooling_tower.working_sound.sound.volume = 0.8
hybrid_cooling_tower.working_sound.idle_sound = { filename = "__base__/sound/idle1.ogg", volume = 0.6 }
hybrid_cooling_tower.working_sound.apparent_volume = 1.5
hybrid_cooling_tower.module_specification = nil
hybrid_cooling_tower.allowed_effects = nil
hybrid_cooling_tower.energy_usage = "1W"
hybrid_cooling_tower.energy_source = {}
hybrid_cooling_tower.energy_source.type = 'void'
hybrid_cooling_tower.animation = animation

hybrid_cooling_tower.working_visualisations = {}
hybrid_cooling_tower.working_visualisations[1] = {}
hybrid_cooling_tower.working_visualisations[1].apply_recipe_tint = "tertiary"
hybrid_cooling_tower.working_visualisations[1].fadeout = true
hybrid_cooling_tower.working_visualisations[1].constant_speed = true
hybrid_cooling_tower.working_visualisations[1].north_position = util.by_pixel_hr(0, 0)
hybrid_cooling_tower.working_visualisations[1].east_position = util.by_pixel_hr(0, 0)
hybrid_cooling_tower.working_visualisations[1].south_position = util.by_pixel_hr(0, 0)
hybrid_cooling_tower.working_visualisations[1].west_position = util.by_pixel_hr(0, 0)
hybrid_cooling_tower.working_visualisations[1].render_layer = "wires"
hybrid_cooling_tower.working_visualisations[1].animation = {}
hybrid_cooling_tower.working_visualisations[1].animation.filename = "__base__/graphics/entity/chemical-plant/chemical-plant-smoke-outer.png"
hybrid_cooling_tower.working_visualisations[1].animation.frame_count = 47
hybrid_cooling_tower.working_visualisations[1].animation.line_length = 16
hybrid_cooling_tower.working_visualisations[1].animation.width = 46
hybrid_cooling_tower.working_visualisations[1].animation.height = 94
hybrid_cooling_tower.working_visualisations[1].animation.animation_speed = 0.5
hybrid_cooling_tower.working_visualisations[1].animation.shift = util.by_pixel(-2, -40)
hybrid_cooling_tower.working_visualisations[1].animation.hr_version = {}
hybrid_cooling_tower.working_visualisations[1].animation.hr_version.filename = "__base__/graphics/entity/chemical-plant/hr-chemical-plant-smoke-outer.png"
hybrid_cooling_tower.working_visualisations[1].animation.hr_version.frame_count = 47
hybrid_cooling_tower.working_visualisations[1].animation.hr_version.line_length = 16
hybrid_cooling_tower.working_visualisations[1].animation.hr_version.width = 90
hybrid_cooling_tower.working_visualisations[1].animation.hr_version.height = 188
hybrid_cooling_tower.working_visualisations[1].animation.hr_version.animation_speed = 0.5
hybrid_cooling_tower.working_visualisations[1].animation.hr_version.shift = util.by_pixel(-2, -40)
hybrid_cooling_tower.working_visualisations[1].animation.hr_version.scale = 0.5

hybrid_cooling_tower.fluid_boxes = {}
hybrid_cooling_tower.fluid_boxes[1] = {}
hybrid_cooling_tower.fluid_boxes[1].production_type = "input"
--hybrid_cooling_tower.fluid_boxes[1].pipe_picture = apm.lib.utils.pipecovers.assembler1pipepictures()
hybrid_cooling_tower.fluid_boxes[1].pipe_covers = apm.lib.utils.pipecovers.pipecoverspictures()
hybrid_cooling_tower.fluid_boxes[1].base_area = 10
hybrid_cooling_tower.fluid_boxes[1].base_level = 0
hybrid_cooling_tower.fluid_boxes[1].pipe_connections = {{ type="input-output", position = {0, 1} }}
--hybrid_cooling_tower.fluid_boxes[1].secondary_draw_orders = { north = -1 }
--hybrid_cooling_tower.fluid_boxes[2] = table.deepcopy(hybrid_cooling_tower.fluid_boxes[1])
--hybrid_cooling_tower.fluid_boxes[2].pipe_connections = {{ type="input-output", position = {0, -1} }}
--hybrid_cooling_tower.fluid_boxes[3] = table.deepcopy(hybrid_cooling_tower.fluid_boxes[1])
--hybrid_cooling_tower.fluid_boxes[3].pipe_connections = {{ type="input", position = {1, 0} }}
--hybrid_cooling_tower.fluid_boxes[4] = table.deepcopy(hybrid_cooling_tower.fluid_boxes[1])
--hybrid_cooling_tower.fluid_boxes[4].pipe_connections = {{ type="input", position = {-1, 0} }}
data:extend({hybrid_cooling_tower})

-- Recipe ---------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local item_icon_a = apm.lib.utils.icon.get.from_fluid('steam')
--local item_icon_b = {apm.lib.icons.dynamics.cooling}
local item_icon_b = {apm.lib.icons.dynamics.temp_down}
local icons = apm.lib.utils.icon.merge({item_icon_a, item_icon_b})


local recipe = {}
recipe.type = "recipe"
recipe.name = "apm_vent_steam"
recipe.category = 'apm_gas_vent'
recipe.icons = icons
recipe.group = "apm_nuclear"
recipe.subgroup = "apm_nuclear_cooling_tower"
recipe.order = 'ab_a'
recipe.crafting_machine_tint = {
        primary = {r = 1.000, g = 1.000, b = 1.000, a = 0.500},
        secondary = {r = 1.000, g = 1.000, b = 1.000, a = 0.500},
        tertiary = {r = 1.000, g = 1.000, b = 1.000, a = 0.500},
        quaternary = {r = 1.000, g = 1.000, b = 1.000, a = 0.500},
    }
recipe.normal = {}
recipe.normal.enabled = true
recipe.normal.energy_required = 5
recipe.normal.ingredients = {
        {type="fluid", name="steam", amount=80}
    }
recipe.normal.results = {}
recipe.normal.main_product = ''
recipe.normal.requester_paste_multiplier = 4
recipe.normal.always_show_products = true
recipe.normal.always_show_made_in = true
--recipe.normal.allow_decomposition = false
--recipe.normal.allow_as_intermediate = false
--recipe.normal.allow_intermediates = false
recipe.expensive = table.deepcopy(recipe.normal)
--recipe.expensive.energy_required = 4
--recipe.expensive.ingredients = {}
--recipe.expensive.results = {}
data:extend({recipe})